Phasespace Examples
===

How to use the Phasespace repo with AlloSystem

Initializing
===

To intialize the project you need to run:

	./init


Building
===

To build and run a file, you can do:

	./run Phasespace/examples/PickRay.cpp


